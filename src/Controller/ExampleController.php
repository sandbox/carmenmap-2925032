<?php

namespace Drupal\moduloSandbox\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class ExampleController extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build = [
      '#type' => 'markup',
      '#markup' => 'Hola Carmen!',
    ];

  return $build;

  }

}
